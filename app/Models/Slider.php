<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Slider extends Model
{
    use HasFactory;

    protected $fillable = ['title','description','image'];

    protected $hidden = ['image'];

    protected $appends = ['image_url'];

    protected function getImageUrlAttribute()
    {
        return Storage::disk('public')->url($this->image);
    }
}
