<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Portfolio extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'image', 'category'
    ];

    protected $appends = ['image_url'];

    protected function getImageUrlAttribute()
    {
        return Storage::disk('public')->url($this->image);
    }
}
