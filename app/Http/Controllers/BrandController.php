<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::latest()->paginate(5);
        return view('admin.brand', compact('brands'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:brands',
            'image' => 'required|mimes:jpg,png,jpeg'
        ]);

        $name = time() . Str::random(10) . '.' . $request->file('image')->getClientOriginalExtension();
        $path = 'brand' . '/' . $name;


        Storage::disk('public')->put($path, Image::make($request->file('image'))->resize(300, 200)->encode());


        Brand::create([
            'name' => $request->name,
            'image' => $path
        ]);

        return redirect()->back()->with('success', 'Brand created successfully');

    }

    public function update(Request $request, Brand $brand)
    {
        $request->validate([
            'name' => 'required|unique:brands,name,' . $brand->id,
            'image' => 'nullable|mimes:jpg,png,jpeg'
        ]);

        $path = $brand->image;

        if ($request->hasFile('image')) {

            if (Storage::disk('public')->exists($brand->image)) {
                Storage::disk('public')->delete($brand->image);
            }

            $name = time() . Str::random(10) . '.' . $request->file('image')->getClientOriginalExtension();
            $path = 'brand' . '/' . $name;


            Storage::disk('public')->put($path, Image::make($request->file('image'))->resize(300, 200)->encode());

        }

        $brand->update([
            'name' => $request->name,
            'image' => $path
        ]);

        return redirect()->back()->with('success', 'Brand updated successfully');
    }

    public function delete(Brand $brand)
    {
        if (Storage::disk('public')->exists($brand->image)) {
            Storage::disk('public')->delete($brand->image);
        }

        $brand->delete();

        return redirect()->back()->with('success', 'Brand deleted successfully');

    }
}
