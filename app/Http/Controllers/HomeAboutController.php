<?php

namespace App\Http\Controllers;

use App\Models\HomeAbout;
use Illuminate\Http\Request;

class HomeAboutController extends Controller
{
    public function index()
    {
        $homeAbout = HomeAbout::first();
        return view('admin.about', compact('homeAbout'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'short_dis' => 'required|min:50|max:150',
            'long_dis' => 'required|min:100|max:500',
        ]);

        $homeAbout = HomeAbout::first();
        if ($homeAbout) {
            $homeAbout->update([
                'title' => $request->title,
                'short_dis' => $request->short_dis,
                'long_dis' => $request->long_dis,
            ]);
        } else {
            HomeAbout::create([
                'title' => $request->title,
                'short_dis' => $request->short_dis,
                'long_dis' => $request->long_dis,
            ]);
        }


        return redirect()->back()->with('success', 'Changes saved successfully');
    }


}
