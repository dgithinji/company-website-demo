<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function index()
    {
        $contact = Contact::first();
        return view('admin.contact', compact('contact'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'address' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        $contact = Contact::first();

        if ($contact) {
            $contact->update([
                'address' => $request->address,
                'email' => $request->email,
                'phone' => $request->phone,
            ]);
        } else {
            Contact::create([
                'address' => $request->address,
                'email' => $request->email,
                'phone' => $request->phone,
            ]);
        }


        return redirect()->back()->with('success', 'contact updated successfully');

    }

}
