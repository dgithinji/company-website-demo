<?php

namespace App\Http\Controllers;

use App\Models\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PortfolioController extends Controller
{
    public function index()
    {
        $portfolios = Portfolio::latest()->paginate(5);
        return view('admin.portfolio', compact('portfolios'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'category' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg'
        ]);

        $path = Storage::disk('public')->putFile('portfolio', $request->file('image'));

        Portfolio::create([
            'title' => $request->title,
            'category' => $request->category,
            'image' => $path
        ]);

        return redirect()->back()->with('success', 'Portfolio created successfully');

    }

    public function update(Request $request, Portfolio $portfolio)
    {
        $request->validate([
            'title' => 'required',
            'category' => 'required',
            'image' => 'nullable|mimes:jpg,png,jpeg'
        ]);

        $path = $portfolio->image;

        if ($request->hasFile('image')) {

            if (Storage::exists($portfolio->image)) {
                Storage::delete($portfolio->image);
            }

            $path = Storage::disk('public')->putFile('portfolio', $request->file('image'));
        }

        $portfolio->update([
            'title' => $request->title,
            'category' => $request->category,
            'image' => $path
        ]);

        return redirect()->back()->with('success', 'Portfolio updated successfully');

    }

    public function destroy(Portfolio $portfolio)
    {
        if (Storage::exists($portfolio->image)) {
            Storage::delete($portfolio->image);
        }

        $portfolio->delete();

        return redirect()->back()->with('success', 'Portfolio deleted successfully');
    }
}
