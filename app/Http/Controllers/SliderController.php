<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::latest()->paginate(5);

        return view('admin.slider', compact('sliders'));

    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:sliders',
            'description' => 'required|min:50|max:250',
            'image' => 'required|mimes:jpg,png,jpeg'
        ]);

        $name = time() . Str::random(10) . '.' . $request->file('image')->getClientOriginalExtension();
        $path = 'slider' . '/' . $name;


        Storage::disk('public')->put($path, Image::make($request->file('image'))->resize(1920, 1088)->encode());


        Slider::create([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $path
        ]);

        return redirect()->back()->with('success', 'Slider created successfully');
    }

    public function update(Request $request, Slider $slider)
    {
        $request->validate([
            'title' => 'required|unique:brands,title,' . $slider->id,
            'description' => 'required|min:50|max:250',
            'image' => 'nullable|mimes:jpg,png,jpeg'
        ]);

        $path = $slider->image;

        if ($request->hasFile('image')) {

            if (Storage::disk('public')->exists($slider->image)) {
                Storage::disk('public')->delete($slider->image);
            }

            $name = time() . Str::random(10) . '.' . $request->file('image')->getClientOriginalExtension();
            $path = 'slider' . '/' . $name;


            Storage::disk('public')->put($path, Image::make($request->file('image'))->resize(1920, 1088)->encode());

        }

        $slider->update([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $path
        ]);

        return redirect()->back()->with('success', 'Slider updated successfully');
    }


    public function delete(Slider $slider)
    {
        if (Storage::disk('public')->exists($slider->image)) {
            Storage::disk('public')->delete($slider->image);
        }

        $slider->delete();

        return redirect()->back()->with('success', 'Slider deleted successfully');
    }
}
