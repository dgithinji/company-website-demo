<?php

namespace App\Providers;

use App\Models\Brand;
use App\Models\Contact;
use App\Models\HomeAbout;
use App\Models\Portfolio;
use App\Models\Slider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer(['contact', 'layouts.body.footer'], function ($view) {
            $contact = Contact::first();
            $view->with(['contact' => $contact]);
        });

        View::composer(['home', 'portfolio'], function ($view) {
            $brands = Brand::latest()->get();
            $sliders = Slider::all();
            $homeAbout = HomeAbout::first();
            $portfolios = Portfolio::all();
            $view->with(['brands' => $brands, 'sliders' => $sliders, 'homeabout' => $homeAbout, 'portfolios' => $portfolios]);
        });
    }
}
