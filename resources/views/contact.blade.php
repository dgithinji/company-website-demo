@extends('layouts.master_home')
@section('main_content')
    <main id="main">
        @include('layouts.body.nav',['name'=> 'contact'])

        <div class="map-section">
            <iframe style="border:0; width: 100%; height: 350px;"
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621"
                    frameborder="0" allowfullscreen></iframe>
        </div>

        <section id="contact" class="contact">
            <div class="container">

                <div class="row justify-content-center" data-aos="fade-up">

                    <div class="col-lg-10">

                        <div class="info-wrap">
                            <div class="row">
                                <div class="col-lg-4 info">
                                    <i class="icofont-google-map"></i>
                                    <h4>Location:</h4>
                                    <p>{{$contact->address}}</p>
                                </div>

                                <div class="col-lg-4 info mt-4 mt-lg-0">
                                    <i class="icofont-envelope"></i>
                                    <h4>Email:</h4>
                                    <p>{{$contact->email}}</p>
                                </div>

                                <div class="col-lg-4 info mt-4 mt-lg-0">
                                    <i class="icofont-phone"></i>
                                    <h4>Call:</h4>
                                    <p>{{$contact->phone}}</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row mt-5 justify-content-center" data-aos="fade-up">
                    <div class="col-lg-10">
                        <form action="{{route('create.message')}}" method="post" class="email-form">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-6 form-group">
                                    <input type="text" name="name"
                                           class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           id="name"
                                           placeholder="Your Name"/>
                                    @if($errors->has('name'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('name')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-6 form-group">
                                    <input type="email"
                                           class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email"
                                           id="email"
                                           placeholder="Your Email"/>
                                    @if($errors->has('email'))
                                        <div class="text-danger">
                                            {{ $errors->first('email')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text"
                                       class="form-control {{ $errors->has('subject') ? ' is-invalid' : '' }}"
                                       name="subject" id="subject"
                                       placeholder="Subject"/>
                                @if($errors->has('subject'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('subject')}}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <textarea class="form-control {{ $errors->has('message') ? ' is-invalid' : '' }}"
                                          name="message" rows="5" placeholder="Message"></textarea>
                                @if($errors->has('message'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('message')}}
                                    </div>
                                @endif
                            </div>
                            <div class="mb-3">
                                @if(\Illuminate\Support\Facades\Session::has('success'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        <strong>Success !</strong> {{session('success')}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @elseif(\Illuminate\Support\Facades\Session::has('errors'))
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <strong>Error !</strong> Please check form for errors
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                            </div>
                            <div class="text-center">
                                <button class="btn btn-success" type="submit">Send Message</button>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </section>
    </main>
@endsection
