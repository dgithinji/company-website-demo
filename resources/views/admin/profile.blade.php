@extends('admin.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="breadcrumb-wrapper">
                <h1>Profile</h1>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0">
                        <li class="breadcrumb-item">
                            <a href="{{route('dashboard')}}">
                                <span class="mdi mdi-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Profile</li>
                    </ol>
                </nav>

            </div>
            <div class="row">
                <div class="col-12">
                    <!-- Recent Order Table -->
                    <div class="card card-table-border-none" id="recent-orders">
                        <div class="card-header justify-content-between">
                            <h2>Profile</h2>

                        </div>
                        <div class="card-body pt-0 pb-5">
                            <form action="{{route('user-profile-information.update')}}" method="POST" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf

                                <div class="form-group">
                                    <label for="photo" class="col-form-label">Photo</label>
                                    <div class="mb-2">
                                        <img src="{{\Illuminate\Support\Facades\Auth::user()->profile_photo_url}}"
                                             class="img-thumbnail" style="height: 100px" alt="">
                                    </div>
                                    <input type="file"
                                           class="form-control-file {{ $errors->updateProfileInformation->has('photo') ? ' is-invalid' : '' }}"
                                           name="photo"
                                           id="photo">
                                    @if($errors->updateProfileInformation->has('photo'))
                                        <div class="invalid-feedback">
                                            {{ $errors->updateProfileInformation->first('photo')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Name</label>
                                    <input type="text"
                                           class="form-control {{ $errors->updateProfileInformation->has('name') ? ' is-invalid' : '' }}"
                                           name="name"
                                           value="{{\Illuminate\Support\Facades\Auth::user()->name}}"
                                           id="email">
                                    @if($errors->updateProfileInformation->has('name'))
                                        <div class="invalid-feedback">
                                            {{ $errors->updateProfileInformation->first('name')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email</label>
                                    <input type="text"
                                           name="email"
                                           id="email"
                                           value="{{\Illuminate\Support\Facades\Auth::user()->email}}"
                                           class="form-control {{ $errors->updateProfileInformation->has('email') ? ' is-invalid' : '' }}">
                                    @if($errors->updateProfileInformation->has('email'))
                                        <div class="invalid-feedback">
                                            {{ $errors->updateProfileInformation->first('email')}}
                                        </div>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Save
                                        changes
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
