@extends('admin.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="breadcrumb-wrapper">
                <h1>Portfolio</h1>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0">
                        <li class="breadcrumb-item">
                            <a href="{{route('dashboard')}}">
                                <span class="mdi mdi-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Portfolio</li>
                    </ol>
                </nav>

            </div>
            <div class="row">
                <div class="col-12">
                    <!-- Recent Order Table -->
                    <div class="card card-table-border-none" id="recent-orders">
                        <div class="card-header justify-content-between">
                            <h2>Portfolio</h2>
                            <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#exampleModalCenter"
                                    data-backdrop="static" data-keyboard="false">Add Portfolio
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Add Portfolio</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{route('create.portfolio')}}" method="POST"
                                              enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="slider-title" class="col-form-label">Portfolio
                                                        Name</label>
                                                    <input type="text"
                                                           class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                           name="title"
                                                           id="slider-title">
                                                    @if($errors->has('title'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('title')}}
                                                        </div>
                                                    @endif

                                                </div>
                                                <div class="form-group">
                                                    <label for="category" class="col-form-label">Select Category</label>
                                                    <select id="category" name="category"
                                                            class="form-control {{ $errors->has('category') ? ' is-invalid' : '' }}">
                                                        <option hidden>Select Category</option>
                                                        <option value="mobile">Mobile</option>
                                                        <option value="web">Web</option>
                                                        <option value="other">Other</option>
                                                    </select>
                                                    @if($errors->has('category'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('category')}}
                                                        </div>
                                                    @endif

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Portfolio Image</label>
                                                    <input type="file"
                                                           class="form-control-file {{ $errors->has('image') ? ' is-invalid' : '' }}"
                                                           name="image">
                                                    @if($errors->has('image'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('image')}}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Close
                                                </button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-body pt-0 pb-5">
                            <table class="table card-table table-responsive table-responsive-large" style="width:100%">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Title</th>
                                    <th class="d-none d-md-table-cell">Category</th>
                                    <th class="d-none d-md-table-cell">Image</th>
                                    <th class="d-none d-md-table-cell">Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($portfolios as $portfolio)
                                    <tr>
                                        <td>{{$portfolios->firstItem() + ($loop->index)}}</td>
                                        <td>
                                            {{$portfolio->title}}
                                        </td>
                                        <td>
                                            {{$portfolio->category}}
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <img src="{{$portfolio->image_url}}" class="img-thumbnail" width="100px"
                                                 height="100px" alt="">
                                        </td>
                                        <td class="d-none d-md-table-cell">{{$portfolio->created_at->diffForHumans()}}</td>
                                        <td class="d-none d-md-table-cell">
                                            <button class="btn btn-primary btn-sm mr-2" data-toggle="modal"
                                                    data-target="{{'#editBrand'.$portfolio->id}}"
                                                    data-backdrop="static" data-keyboard="false">Edit
                                            </button>
                                            <div class="modal fade" id="{{'editBrand'.$portfolio->id}}" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalCenterTitle">Add
                                                                Brand</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('update.portfolio',[$portfolio->id])}}"
                                                              method="POST"
                                                              enctype="multipart/form-data">
                                                            @csrf
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label for="slider-title" class="col-form-label">Portfolio
                                                                        Name</label>
                                                                    <input type="text"
                                                                           class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                                           name="title"
                                                                           value="{{$portfolio->title}}"
                                                                           id="slider-title">
                                                                    @if($errors->has('title'))
                                                                        <div class="invalid-feedback">
                                                                            {{ $errors->first('title')}}
                                                                        </div>
                                                                    @endif

                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="category" class="col-form-label">Select
                                                                        Category</label>
                                                                    <select id="category" name="category"
                                                                            class="form-control {{ $errors->has('category') ? ' is-invalid' : '' }}">
                                                                        <option hidden value="">Select Category
                                                                        </option>
                                                                        <option value="mobile"
                                                                                @if($portfolio->category == 'mobile') selected @endif>
                                                                            Mobile
                                                                        </option>
                                                                        <option value="web"
                                                                                @if($portfolio->category == 'web') selected @endif>
                                                                            Web
                                                                        </option>
                                                                        <option value="other"
                                                                                @if($portfolio->category == 'other') selected @endif>
                                                                            Other
                                                                        </option>
                                                                    </select>
                                                                    @if($errors->has('category'))
                                                                        <div class="invalid-feedback">
                                                                            {{ $errors->first('category')}}
                                                                        </div>
                                                                    @endif

                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Portfolio Image</label>
                                                                    <input type="file"
                                                                           class="form-control-file {{ $errors->has('image') ? ' is-invalid' : '' }}"
                                                                           name="image">
                                                                    @if($errors->has('image'))
                                                                        <div class="invalid-feedback">
                                                                            {{ $errors->first('image')}}
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">
                                                                    Close
                                                                </button>
                                                                <button type="submit" class="btn btn-primary">Save
                                                                    changes
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="btn btn-danger btn-sm"
                                               onclick="deleteBrand('{{route('delete.portfolio',[$portfolio->id])}}')">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>No Portfolio found</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            {{$portfolios->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function deleteBrand(url) {
            Swal.fire({
                title: 'Are you sure?',
                text: "Slider will be deleted!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }
    </script>

@endsection
