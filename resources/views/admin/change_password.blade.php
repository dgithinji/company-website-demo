@extends('admin.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="breadcrumb-wrapper">
                <h1>Change Password</h1>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0">
                        <li class="breadcrumb-item">
                            <a href="{{route('dashboard')}}">
                                <span class="mdi mdi-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Change Password</li>
                    </ol>
                </nav>

            </div>
            <div class="row">
                <div class="col-12">
                    <!-- Recent Order Table -->
                    <div class="card card-table-border-none" id="recent-orders">
                        <div class="card-header justify-content-between">
                            <h2>Change Password</h2>

                        </div>
                        <div class="card-body pt-0 pb-5">
                            <form action="{{route('user-password.update')}}" method="POST"
                                  enctype="multipart/form-data">
                                @method('PUT')
                                @csrf

                                <div class="form-group">
                                    <label for="email" class="col-form-label">Current Password</label>
                                    <input type="password"
                                           class="form-control {{ $errors->updatePassword->has('current_password') ? ' is-invalid' : '' }}"
                                           name="current_password"
                                           id="email">
                                    @if($errors->updatePassword->has('current_password'))
                                        <div class="invalid-feedback">
                                            {{ $errors->updatePassword->first('current_password')}}
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-form-label">New Password</label>
                                    <input type="text"
                                           class="form-control {{ $errors->updatePassword->has('password') ? ' is-invalid' : '' }}"
                                           name="password"
                                           id="email">
                                    @if($errors->updatePassword->has('password'))
                                        <div class="invalid-feedback">
                                            {{ $errors->updatePassword->first('password')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Confirm Password</label>
                                    <input type="text"
                                           name="password_confirmation"
                                           id="email"
                                           class="form-control {{ $errors->updatePassword->has('password_confirmation') ? ' is-invalid' : '' }}">
                                    @if($errors->updatePassword->has('password_confirmation'))
                                        <div class="invalid-feedback">
                                            {{ $errors->updatePassword->first('password_confirmation')}}
                                        </div>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Save
                                        changes
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
