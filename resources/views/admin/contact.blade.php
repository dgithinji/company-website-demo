@extends('admin.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="breadcrumb-wrapper">
                <h1>Contacts</h1>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0">
                        <li class="breadcrumb-item">
                            <a href="{{route('dashboard')}}">
                                <span class="mdi mdi-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Contacts</li>
                    </ol>
                </nav>

            </div>
            <div class="row">
                <div class="col-12">
                    <!-- Recent Order Table -->
                    <div class="card card-table-border-none" id="recent-orders">
                        <div class="card-header justify-content-between">
                            <h2>Contacts</h2>

                        </div>
                        <div class="card-body pt-0 pb-5">
                            <form action="{{route('update.contact')}}" method="POST">
                                @csrf

                                <div class="form-group">
                                    <label for="address" class="col-form-label">Address</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}"
                                           name="address"
                                           value="{{empty($contact->address) ? '' : $contact->address}}"
                                           id="address">
                                    @if($errors->has('address'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('address')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-form-label">Email</label>
                                    <input type="email"
                                           class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email"
                                           value="{{empty($contact->email) ? '' : $contact->email}}"
                                           id="email">
                                    @if($errors->has('email'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('email')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-form-label">Phone</label>
                                    <input type="text"
                                           name="phone"
                                           id="phone"
                                           value="{{empty($contact->phone) ? '' : $contact->phone}}"
                                           class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}">
                                    @if($errors->has('phone'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('phone')}}
                                        </div>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Save
                                        changes
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
