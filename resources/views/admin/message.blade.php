@extends('admin.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="breadcrumb-wrapper">
                <h1>Message</h1>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0">
                        <li class="breadcrumb-item">
                            <a href="{{route('dashboard')}}">
                                <span class="mdi mdi-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Message</li>
                    </ol>
                </nav>

            </div>
            <div class="row">
                <div class="col-12">
                    <!-- Recent Order Table -->
                    <div class="card card-table-border-none" id="recent-orders">
                        <div class="card-header justify-content-between">
                            <h2>Message</h2>
                        </div>
                        <div class="card-body pt-0 pb-5">
                            <table class="table card-table table-responsive table-responsive-large" style="width:100%">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Name</th>
                                    <th class="d-none d-md-table-cell">Email</th>
                                    <th class="d-none d-md-table-cell">Subject</th>
                                    <th class="d-none d-md-table-cell">Message</th>
                                    <th class="d-none d-md-table-cell">Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($messages as $message)
                                    <tr>
                                        <td>{{$messages->firstItem() + ($loop->index)}}</td>
                                        <td>
                                            {{$message->name}}
                                        </td>
                                        <td>
                                            {{$message->email}}
                                        </td>
                                        <td>
                                            {{$message->subject}}
                                        </td>
                                        <td>
                                            {{$message->message}}
                                        </td>
                                        <td class="d-none d-md-table-cell">{{$message->created_at->diffForHumans()}}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>No Messages found</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            {{$messages->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
