@extends('admin.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="breadcrumb-wrapper">
                <h1>About Us</h1>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0">
                        <li class="breadcrumb-item">
                            <a href="{{route('dashboard')}}">
                                <span class="mdi mdi-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">About Us</li>
                    </ol>
                </nav>

            </div>
            <div class="row">
                <div class="col-12">
                    <!-- Recent Order Table -->
                    <div class="card card-table-border-none" id="recent-orders">
                        <div class="card-header justify-content-between">
                            <h2>About Us</h2>

                        </div>
                        <div class="card-body pt-0 pb-5">

                            <form action="{{route('about.update')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="homeAbout-title" class="col-form-label">Title</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                           name="title"
                                           value="{{empty($homeAbout->title) ? '' : $homeAbout->title}}"
                                           id="homeAbout-title">
                                    @if($errors->has('title'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('title')}}
                                        </div>
                                    @endif

                                </div>
                                <div class="form-group">
                                    <label for="short_dis" class="col-form-label">Short Desc</label>
                                    <textarea name="short_dis"
                                              id="short_dis"
                                              class="form-control {{ $errors->has('short_dis') ? ' is-invalid' : '' }}"
                                              rows="3">{{empty($homeAbout->short_dis) ? '' :$homeAbout->short_dis}}</textarea>
                                    @if($errors->has('short_dis'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('short_dis')}}
                                        </div>
                                    @endif

                                </div>
                                <div class="form-group">
                                    <label for="long_dis" class="col-form-label">Long Desc</label>
                                    <textarea name="long_dis"
                                              id="long_dis"
                                              class="form-control {{ $errors->has('long_dis') ? ' is-invalid' : '' }}"
                                              rows="5">{{empty($homeAbout->long_dis) ? '' : $homeAbout->long_dis}}</textarea>
                                    @if($errors->has('long_dis'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('long_dis')}}
                                        </div>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Save
                                        changes
                                    </button>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
