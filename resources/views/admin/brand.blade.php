@extends('admin.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="breadcrumb-wrapper">
                <h1>Brands</h1>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0">
                        <li class="breadcrumb-item">
                            <a href="{{route('dashboard')}}">
                                <span class="mdi mdi-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">Brands</li>
                    </ol>
                </nav>

            </div>
            <div class="row">
                <div class="col-12">
                    <!-- Recent Order Table -->
                    <div class="card card-table-border-none" id="recent-orders">
                        <div class="card-header justify-content-between">
                            <h2>Brands</h2>
                            <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#exampleModalCenter"
                                    data-backdrop="static" data-keyboard="false">Add Brand
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Add Brand</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{route('create.brand')}}" method="POST"
                                              enctype="multipart/form-data">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="brand-name" class="col-form-label">Brand Name</label>
                                                    <input type="text"
                                                           class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                           name="name"
                                                           id="brand-name">
                                                    @if($errors->has('name'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('name')}}
                                                        </div>
                                                    @endif

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Brand Image</label>
                                                    <input type="file"
                                                           class="form-control-file {{ $errors->has('image') ? ' is-invalid' : '' }}"
                                                           name="image">
                                                    @if($errors->has('image'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('image')}}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Close
                                                </button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-body pt-0 pb-5">
                            <table class="table card-table table-responsive table-responsive-large" style="width:100%">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Brand Name</th>
                                    <th class="d-none d-md-table-cell">Brand Image</th>
                                    <th class="d-none d-md-table-cell">Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($brands as $brand)
                                    <tr>
                                        <td>{{$brands->firstItem() + ($loop->index)}}</td>
                                        <td>
                                            {{$brand->name}}
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <img src="{{$brand->image_url}}" class="img-thumbnail" width="100px"
                                                 height="100px" alt="">
                                        </td>
                                        <td class="d-none d-md-table-cell">{{$brand->created_at->diffForHumans()}}</td>
                                        <td class="d-none d-md-table-cell">
                                            <button class="btn btn-primary btn-sm mr-2" data-toggle="modal"
                                                    data-target="{{'#editBrand'.$brand->id}}"
                                                    data-backdrop="static" data-keyboard="false">Edit
                                            </button>
                                            <div class="modal fade" id="{{'editBrand'.$brand->id}}" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalCenterTitle">Add
                                                                Brand</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('update.brand',[$brand->id])}}"
                                                              method="POST"
                                                              enctype="multipart/form-data">
                                                            @csrf
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label for="brand-name" class="col-form-label">Brand
                                                                        Name</label>
                                                                    <input type="text"
                                                                           class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                                           name="name"
                                                                           value="{{$brand->name}}"
                                                                           id="brand-name">
                                                                    @if($errors->has('name'))
                                                                        <div class="invalid-feedback">
                                                                            {{ $errors->first('name')}}
                                                                        </div>
                                                                    @endif

                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Brand Image</label>
                                                                    <input type="file"
                                                                           class="form-control-file {{ $errors->has('image') ? ' is-invalid' : '' }}"
                                                                           name="image">
                                                                    @if($errors->has('image'))
                                                                        <div class="invalid-feedback">
                                                                            {{ $errors->first('image')}}
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">
                                                                    Close
                                                                </button>
                                                                <button type="submit" class="btn btn-primary">Save
                                                                    changes
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="btn btn-danger btn-sm"
                                               onclick="deleteBrand('{{route('delete.brand',[$brand->id])}}')">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>No brands found</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            {{$brands->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function deleteBrand(url) {
            Swal.fire({
                title: 'Are you sure?',
                text: "Brand will be deleted!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }
    </script>

@endsection
