@extends('admin.admin_master')
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <!-- Top Statistics -->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card card-mini mb-4">
                        <div class="card-body">
                            <h2 class="mb-1">Easy Dashboard</h2>
                            <p class="my-2">Welcome {{\Illuminate\Support\Facades\Auth::user()->name}}</p>
                            <p>{{date("F j, Y, g:i a")}}</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
@endsection
