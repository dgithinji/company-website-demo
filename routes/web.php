<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ContactFormController;
use App\Http\Controllers\HomeAboutController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\SliderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/about', function () {
    return view('about');
})->name('home.about');

Route::get('/services', function () {
    return view('services');
})->name('home.services');

Route::get('/portfolio', function () {
    return view('portfolio');
})->name('home.portfolio');

Route::get('/pricing', function () {
    return view('pricing');
})->name('home.pricing');

Route::get('/blog', function () {
    return view('blog');
})->name('home.blog');

Route::get('/contact', function () {
    return view('contact');
})->name('home.contact');


Route::post('/create-message', [ContactFormController::class, 'store'])->name('create.message');


Route::middleware(['auth:sanctum'])->prefix('dashboard')->group(function () {
    Route::get('/', function () {
        return view('admin.index');
    })->name('dashboard');

    Route::get('/profile', function () {
        return view('admin.profile');
    })->name('profile');

    Route::get('/change_password', function () {
        return view('admin.change_password');
    })->name('change_password');

    //brands
    Route::get('/brands', [BrandController::class, 'index'])->name('brands');
    Route::post('/create-brand', [BrandController::class, 'store'])->name('create.brand');
    Route::post('/update-brand/{brand}', [BrandController::class, 'update'])->name('update.brand');
    Route::get('/delete-brand/{brand}', [BrandController::class, 'delete'])->name('delete.brand');

    //slider
    Route::get('/sliders', [SliderController::class, 'index'])->name('sliders');
    Route::post('/create-slider', [SliderController::class, 'store'])->name('create.slider');
    Route::post('/update-slider/{slider}', [SliderController::class, 'update'])->name('update.slider');
    Route::get('/delete-slider/{slider}', [SliderController::class, 'delete'])->name('delete.slider');

    //home about
    Route::get('/about', [HomeAboutController::class, 'index'])->name('about');
    Route::post('/about-update', [HomeAboutController::class, 'store'])->name('about.update');

    //portfolio
    Route::get('/portfolio', [PortfolioController::class, 'index'])->name('portfolio');
    Route::post('/create-portfolio', [PortfolioController::class, 'store'])->name('create.portfolio');
    Route::post('/create-portfolio/{portfolio}', [PortfolioController::class, 'update'])->name('update.portfolio');
    Route::get('/delete-portfolio/{portfolio}', [PortfolioController::class, 'destroy'])->name('delete.portfolio');

    //contact
    Route::get('/contact', [ContactController::class, 'index'])->name('contact');
    Route::post('/update-contact', [ContactController::class, 'store'])->name('update.contact');

    //message
    Route::get('/message', [ContactFormController::class, 'index'])->name('message');
});
